/** This will be filled with fake code to understand the process of the event loop, the lifecycle
 * of the entire application.
 */

const pendingTimers = [];
const pendingOSTasks = [];
const pendingOperations = [];

//  node thisFile.js
/** First the code gets executed by node
 * New timers, tasks and operations are recorded from the running file.
 */
thisFile.runContents(); // with pseudo code, a simulation of what happens inside node js.

function shouldContinue() {
  /** Inside here there are 3 checks that the event loop evaluates to decide whether or not it should
   * continue its execution.
   */
  // Check One: Any pending functions setTimeout, setInterval, SetImmediate?
  // Check Two: Any pending OS tasks? (like servers listening for requests to ports).
  // Check Three: Any pending long running operations? (Like fs module).
  return pendingTimers.length || pendingOSTasks.length || pendingOperations.length;
}

/** After all the code is run in the first reading, then we enter the loop
 * The event loop could be represented with a while loop.
 * The body of the while statement will be executed again and again.
 * This iterations in the event loop world are known as ticks.
 *
 * Every time the event loops gets executed it 'ticks'
 * Entire body executes in one tick */

while (shouldContinue()) {
  // 1) Node looks at pendingTimers and sees if any functions are ready to be called.
  // setTimeout, setInterval
  // 2) Node looks at pending OSTasks and pending operation and calls relevant callbacks.
  // 3) Pause execution and sits around waiting for new events occur.
  // - a new pendingOS Task is done
  // - a new pending Operation is done
  // - a timer is about to complete
  // 4) Look at pendingTimers. Call any setImmediate.
  // 5) Handle any 'close' events. (cleanup code in the emitter.on('close', () => {...}) hook)
}

/** Exit back to terminal */
