const timeoutScheduled = Date.now();

setInterval(() => {
  const delay = Date.now() - timeoutScheduled;
  console.log(`${delay / 1000} seconds have passed`);
}, 1000);
